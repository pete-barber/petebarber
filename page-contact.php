<?php
    get_header();
?>

    <div class="wrapper marg contact">

        <h1 class="tc marg"><?php echo get_the_title(get_option('page_for_posts')); ?></h1>
            
            
        
                <?php 

                if (have_posts()) :

                while (have_posts()) : the_post();

                    get_template_part( 'loops/loop', get_post_type() );
                    
                endwhile;


                else : 

                    get_template_part( 'loops/loop', 'none' );

                endif;

                ?>
            
        
    </div>

<?php
    get_footer();
?>