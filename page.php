<?php
    get_header();
?>

    <div class="wrapper marg">

        <h1 class="tc marg"><?php echo get_the_title(get_option('page_for_posts')); ?></h1>

        
            
            <div class="sidebar">
            
                <?php //get_sidebar(); ?>
                
            </div>
            
            <div class="container">
        
                <?php 

                if (have_posts()) :

                while (have_posts()) : the_post();

                    get_template_part( 'loops/loop', get_post_type() );
                    
                endwhile;

                // $pagination = get_the_posts_pagination( array(
                //     'prev_text'          => __( 'Previous page', 'petebarber' ),
                //     'next_text'          => __( 'Next page', 'petebarber' ),
                //     'screen_reader_text' => __( ' ' )
                // ) );
                
                // $pagination = str_replace('<h2 class="screen-reader-text"> </h2>', '', $pagination);

                // echo $pagination;

                else : 

                    get_template_part( 'loops/loop', 'none' );

                endif;

                ?>
            </div>
        
    </div>

<?php
    get_footer();
?>