<?php 
    if( is_front_page() ) : 
        the_content();
    else :
        echo '<div>';
        the_content();
        echo '</div>';
    endif;
?>