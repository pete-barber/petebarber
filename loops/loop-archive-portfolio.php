<article id="post-<?php echo $post->post_name; ?>" class="portfolio-archive-item">

	<a href="<?php echo get_post_permalink(); ?>">
	    <?php
	    if ( has_post_thumbnail() ) :
	        the_post_thumbnail( null, array( 'itemprop' => 'image' ) );
	    endif;
	    ?>
		<?php the_title('<h2 class="post_title tc">','</h2>'); ?>
	</a>

</article>