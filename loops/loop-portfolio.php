
<article class="portfolio" id="post-<?php echo $post->post_name; ?>">
    
    <?php
        if ( has_post_thumbnail() ) :
            the_post_thumbnail( null, array( 'itemprop' => 'image' ) );
        endif;
    ?>

    <div class="portfolio-content white">

        <p>Start Date - <?php the_field('start_date'); ?></p>
        <p>End Date - <?php the_field('end_date'); ?></p>
        <p>Duties Included :</p>
        <?php the_content(); ?>

        
    </div>

    <div class="portfolio-back"></div>

</article>