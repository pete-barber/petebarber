<article id="post-<?php echo $post->post_name; ?>" itemscope itemType="http://schema.org/Blog">
    <?php the_title('<h1 class="post_title">','</h1>'); ?>
    
    <?php
    if ( has_post_thumbnail() ) :
        the_post_thumbnail( null, array( 'itemprop' => 'image' ) );
    endif;
    ?>

    <div itemprop="articleBody">

        <?php the_content(); ?>

        <?php  if ( get_option('add_this_id') ) : ?>
            <div class="addthis_sharing_toolbox"></div>
        <?php endif; ?>
    </div>

</article>