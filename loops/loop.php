<a href="<?php the_permalink(); ?>" class="post">

    <?php

        if ( has_post_thumbnail() ) :
            the_post_thumbnail( 'post' );
        endif;
    ?>

    <div class="post_text">
    <?php 
        the_title('<h2>','</h2>');

        the_excerpt();

        // echo '<span class="post_date">'.get_the_time('jS F Y').'</span>';
     ?>
    </div>
</a>