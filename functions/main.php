<?php

// Enable Main Theme Support
// =============================================================================
function main_support() {
    // Add RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // Enable support for Post Thumbnails, and declare sizes.
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'image', 900, 400, array( 'center', 'top' ) );
    add_image_size( 'portfolio', 500, 500, false );

    //Enable menu support
    register_nav_menus( array(
        'primary'    => 'Primary Menu',
        'anchored'   => 'Anchored Menu',
        'secondary'  => 'Secondary Menu'
    ));

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption', 'post-thumbnails'
    ) );
}

add_action( 'after_setup_theme', 'main_support' );

// Set excerpt length
function new_excerpt_length( $length ) {
  return 26;
}
add_filter( 'excerpt_length', 'new_excerpt_length', 999 );

// Replaces the excerpt "[...]" with custom text or link
function new_excerpt_more($more) {
       global $post;
  // return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full article...</a>';
  return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

// REMOVE WP EMOJI
// ==============================================================================
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Romove Version Number
// ==============================================================================

function remove_version_number() {
  return '';
}
add_filter('the_generator', 'remove_version_number');

// add logo
// ===============================================================================

function logo($type = 'png'){

  //$url = get_stylesheet_directory_uri();
  $template = get_template_directory_uri();
  $base = home_url( '/', null );

  $return = "<a href=$base class='header_logo'><img src='$template/assets/img/logo.$type' /></a>";
  return $return;
}