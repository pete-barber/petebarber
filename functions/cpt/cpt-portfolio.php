<?php 

/**
 * Register a custom post type called "Portfolio".
 *
 * @see get_post_type_labels() for label keys.
 */
function cpt_portfolio_init() {
    $labels = array(
        'name'                  => _x( 'Portfolios', 'Post type general name', 'pete_barber' ),
        'singular_name'         => _x( 'Portfolio', 'Post type singular name', 'pete_barber' ),
        'menu_name'             => _x( 'Portfolios', 'Admin Menu text', 'pete_barber' ),
        'name_admin_bar'        => _x( 'Portfolio', 'Add New on Toolbar', 'pete_barber' ),
        'add_new'               => __( 'Add New', 'pete_barber' ),
        'add_new_item'          => __( 'Add New Portfolio', 'pete_barber' ),
        'new_item'              => __( 'New Portfolio', 'pete_barber' ),
        'edit_item'             => __( 'Edit Portfolio', 'pete_barber' ),
        'view_item'             => __( 'View Portfolio', 'pete_barber' ),
        'all_items'             => __( 'All Portfolios', 'pete_barber' ),
        'search_items'          => __( 'Search Portfolios', 'pete_barber' ),
        'parent_item_colon'     => __( 'Parent Portfolios:', 'pete_barber' ),
        'not_found'             => __( 'No Portfolios found.', 'pete_barber' ),
        'not_found_in_trash'    => __( 'No Portfolios found in Trash.', 'pete_barber' ),
        'featured_image'        => _x( 'Portfolio Image', 'Overrides the “Featured Image” phrase for this post type.', 'pete_barber' ),
        'set_featured_image'    => _x( 'Set portfolio image', 'Overrides the “Set featured image” phrase for this post type.', 'pete_barber' ),
        'remove_featured_image' => _x( 'Remove portfolio image', 'Overrides the “Remove featured image” phrase for this post type.', 'pete_barber' ),
        'use_featured_image'    => _x( 'Use as portfolio image', 'Overrides the “Use as featured image” phrase for this post type.', 'pete_barber' ),
        'archives'              => _x( 'Portfolio archives', 'The post type archive label used in nav menus. Default “Post Archives”.', 'pete_barber' ),
        'insert_into_item'      => _x( 'Insert into portfolio', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).', 'pete_barber' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this portfolio', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).', 'pete_barber' ),
        'filter_items_list'     => _x( 'Filter portfolios list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”.', 'pete_barber' ),
        'items_list_navigation' => _x( 'Portfolios list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”.', 'pete_barber' ),
        'items_list'            => _x( 'Portfolios list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”.', 'pete_barber' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail'),
    );
 
    register_post_type( 'portfolio', $args );
}
 
add_action( 'init', 'cpt_portfolio_init' );

?>