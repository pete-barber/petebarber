<?php 


// Enqueue styles for theme.
// =============================================================================

function add_styles() {

    // remove plugin css

    //wp_dequeue_style( 'wplc-style' );
    //wp_deregister_style( 'wplc-style' );

    //Add plugin CSS
    //wp_register_style( 'wplc-style', plugin_dir_url( __FILE__ ) . '/wp-live-chat-support/css/wplcstyle.css');
    //wp_enqueue_style( 'wplc-style' );
    
    // Load css.
    wp_enqueue_style( 'main-styles', get_template_directory_uri() . '/assets/css/site.css');
}

add_action( 'wp_footer', 'add_styles', 4 );


// Enqueue scripts for theme.
// =============================================================================
function add_scripts() {

    //remove initial verstion of jquery.
    //wp_deregister_script( 'jquery' );
    
    
    
    // Load plug-ins.
    // =========================================================================
    
    // // Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent 
    // $cookiePage = get_page_by_path('cookie-policy');

    // if ( get_field( 'cookie_table', $cookiePage->ID ) ) :
    //     wp_enqueue_script( 'cookie-settings', get_template_directory_uri() . '/assets/js/libs/cookieconsent-setup.js', null, null, false );

    //     wp_enqueue_script( 'cookie-consent', '//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js', 'cookie-settings', null, true );
    // endif;

    // if ( get_option( 'recaptcha_site' ) ) :
    //     wp_enqueue_script( 'google-recaptcha', 'https://www.google.com/recaptcha/api.js', 'jquery', null, false );
    // endif;


    // Load custom jquery.
    wp_enqueue_script( 'main-scripts', get_template_directory_uri() . '/assets/js/site.min.js', null, null, true );

}

add_action( 'wp_enqueue_scripts', 'add_scripts' );

// Begin Custom Scripting to Move JavaScript from the Head to the Footer

function remove_head_scripts() { 
    remove_action('wp_head', 'wp_print_scripts'); 
    remove_action('wp_head', 'wp_print_head_scripts', 9); 
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5); 
} 
add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

// END Custom Scripting to Move JavaScript