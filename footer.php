        </div>

        <footer itemscope="" itemtype="http://schema.org/Organization">
            <div class="main_footer">
                <div class="footer_logo">
                    <?php echo logo('svg'); ?>
                </div>
                <div class="footer_contact white">
                    <h2 class="footer_contact-title">
                        Contact Me
                    </h2>
                    <div class="footer_contact-text">
                        To contact me, please email <a href="mailto:<?php echo get_bloginfo('admin_email'); ?>" class="contact general_link"><?php echo get_bloginfo('admin_email'); ?></a> Please do not contact me for contract work as this is my portfolio of work for perspective full time vacancies.
                    </div>
                </div>
            </div>
            <div class="footer_logos white tc">
                <i class="fab fa-html5"></i>
                <i class="fab fa-css3"></i>
                <i class="fab fa-php"></i>
                <i class="fab fa-wordpress"></i>
            </div>
        </footer>



        <div id="no-script" class="no-jsmsg">
            <input type="checkbox" id="javascript-off-more" name="javascript-off-more" class="hide">
            <p class="notice">You currently have <a href="http://en.wikipedia.org/wiki/Javascript" target="_blank" rel="nofollow">JavaScript</a> disabled! <label for="javascript-off-more" class="btn btn-white btn-sm">read</label></p>
            <div class="javascript-off-more">
                <p class="info">This site requires JavaScript to be enabled. Some functions of the site may not be usable or the site may not look correct until you enable JavaScript. You can enable JavaScript by following <a href="http://www.google.com/support/bin/answer.py?answer=23852" target="_blank" rel="nofollow">this</a> tutorial. Once JavaScript is enabled, this message will be removed.</p>
            </div>
        </div>

        <!--[if lt IE 10]>
            <?php //get_template_part('partials/browser-notice'); ?>
        <![endif]-->

        <?php wp_footer(); ?>

        <?php echo get_option('footer_content') != '' ? get_option('footer_content') : '' ; ?>

    </body>

</html>