<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
   
    <head>

        <meta charset="utf-8">

        <!-- SEO Related-->
        <title><?php if (is_front_page()){
            bloginfo( 'name' );
        }else{
            echo 'Pete Barber ';
            wp_title();
        } ?></title>

        <link rel="sitemap" type="application/xml" title="Sitemap" href="/sitemap_index.xml">

        <!-- Mobile Viewport Options -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <?php echo get_option('social_google') != '' ? '<link rel="publisher" href="https://plus.google.com/'.get_option('social_google').'" />' : '' ; ?>

        <?php wp_head(); ?>

        <!-- Favicon / iOS icons / Android Icons / Windows 8 Tile icons -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/safari-pinned-tab.svg" color="#33383e">
        <meta name="msapplication-TileColor" content="#33383e">
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <?php echo get_option('head_content') != '' ? get_option('head_content') : '' ; ?>
        <?php 
            $size = is_front_page() ? 'full' : 'page' ;
            $id = is_home() || is_category() || is_single() ? get_option('page_for_posts') : (is_404() ? get_option('page_on_front') : $post->ID);
            $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($id), $size ); ?>

    </head>

    
    <body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?> x-ms-format-detection="none">

        <!-- <input type="checkbox" id="menu_checkbox" hidden> -->

        <header class="header" id="header">
            
            <div class="header_container">
                
                <?php echo logo('svg'); ?>

                <?php get_template_part('/partials/menu'); ?>

            </div>
                    
        </header>


        <div class="main_container" <?php 
                                        if (get_post_type( get_the_id()) == 'page'){
                                            echo $backgroundImg[0] ? 'style="background-image: url('.$backgroundImg[0].');"' : '' ; 
                                        }else{
                                            echo 'style="background-image: url('. get_template_directory_uri() .'/assets/img/default.jpg)"';
                                        }
                                    ?>>



            <?php
            if ( function_exists('yoast_breadcrumb') && !is_front_page() ) {
            yoast_breadcrumb('
            <div id="breadcrumbs" class="wrapper">','</div>
            ');
            }
            ?>
        
            
            