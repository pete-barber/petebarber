<?php


// Theme Support
require_once('functions/main.php');

// Theme Scripts
require_once('functions/scripts.php');

// Theme Widgets
//require_once('functions/fn_theme-widgets.php');

// Theme Permissions
//require_once('functions/fn_theme-permissions.php');

// Custom Shortcodes
//require_once('functions/fn_custom_shortcodes/sc_cookies-table.php');
//require_once('functions/fn_custom_shortcodes/sc_testimonials.php');

// Custom Posts
require_once('functions/cpt/cpt-portfolio.php');

// Company Details
//require_once('functions/fn_company-details.php');
//require_once('functions/fn_air-settings.php');

// Event Organiser
//require_once('functions/fn_event-organiser.php');

//require_once('functions/fn_menus-posttypes.php');
