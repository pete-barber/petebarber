<input type="checkbox" name="menu_checkbox" id="menu_checkbox" hidden>

<div class="nav_container">
    <?php
        wp_nav_menu( array('menu' => 'primary', 'menu_class' => 'nav', 'container' => 'ul' ));
    ?>
</div>

<label for="menu_checkbox" class="menu_toggle"></label>