<div class="sidebar" id="menu_fixed">
    <ul class="sidebar_menu">
        <?php $id = is_home() || is_category() || is_single() ? get_option('page_for_posts') : $post->ID ; ?>
        <?php if (is_home() || is_category() || is_single()) :?>
            <li><b><?php echo get_the_title($id); ?></b></li>
        <?php endif; ?>

        <?php
            //if (get_field('sidebar_menu')): 

            //the_field('sidebar_menu');
            
            //else:

            if (is_home() || is_category() || is_single()) :
                $args = array(
                  'title_li' => '',
                  'exclude'  => array( 1 ),
                  'show_option_all' => 'Latest',
                  'show_option_none' => false
                );

                wp_list_categories( $args );

            else :

                if ($post->post_parent) :
                    $ancestors = get_post_ancestors( $post->ID );
                    $parent = $ancestors[count($ancestors)-1];
                    $children = wp_list_pages( array(
                        'title_li' => '',
                        'child_of' => $parent,
                        'echo'     => 0
                    ) );


                    $title = get_the_title( $parent );
                    $link = get_the_permalink( $parent );
                else :
                    $children = wp_list_pages( array(
                        'title_li' => '',
                        'child_of' => $post->ID,
                        'depth'    => 1,
                        'echo'     => 0
                    ) );
                    $title = get_the_title( $post->ID );
                    $link = get_the_permalink( $post->ID );
                endif;

                if ($post->post_parent) : ?>
                    <li class="parent"><b><?php echo get_the_title($parent); ?></b></li>
                <?php endif;

                if (!$post->post_parent) : ?>
                    <li class="no-parent"><b><?php echo get_the_title($id); ?></b></li>
                <?php endif; ?>

                <?php if ( $children ) :
                    echo $children;
                endif;

            endif;
            //endif;
        ?>

    </ul>
</div>
